//
//  SimpleCameraController.swift
//  SimpleCamera
//
//  Created by Simon Ng on 16/10/2016.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import UIKit
import AVFoundation

class SimpleCameraController: UIViewController {

    let captureSession = AVCaptureSession()
    var backingFacingCamera: AVCaptureDevice!
    var frontFacingCamera: AVCaptureDevice!

    var currentDevice: AVCaptureDevice!
    var cameraOutput: AVCapturePhotoOutput!
    var stillImage: UIImage?
    var cameraPreviewLayer: AVCaptureVideoPreviewLayer!

    @IBOutlet var cameraButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        askPermission()

        // Preset the session for taking a photo in full resolution
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        guard let frontFacingCamera = AVCaptureDevice.DiscoverySession.init(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .front).devices.first,
            let backingFacingCamera = AVCaptureDevice.DiscoverySession.init(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .back).devices.first
            else {
                print("No need to continue")
                return
        }

        self.frontFacingCamera = frontFacingCamera
        self.backingFacingCamera = backingFacingCamera

        currentDevice = backingFacingCamera

        cameraOutput = AVCapturePhotoOutput()

        guard let captureDeviceInput = try? AVCaptureDeviceInput(device: backingFacingCamera) else {
            fatalError("NO camera, no need to continue")
        }

        if captureSession.canAddInput(captureDeviceInput) {
            captureSession.addInput(captureDeviceInput)
            if captureSession.canAddOutput(cameraOutput) {
                captureSession.addOutput(cameraOutput!)
                // Provide a camera preview
                cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                view.layer.addSublayer(cameraPreviewLayer)
                cameraPreviewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                cameraPreviewLayer.frame = view.layer.frame
            }
        }

        // Bring the camera button to front
        view.bringSubview(toFront: cameraButton)
        captureSession.startRunning()

        setupGestures()
    }

    fileprivate func setupGestures() {
        let toggleCameraGestureRecognizer = UISwipeGestureRecognizer()
        // Toggle Camera recognizer
        toggleCameraGestureRecognizer.direction = .up
        toggleCameraGestureRecognizer.addTarget(self, action: #selector(toggleCamera))
        view.addGestureRecognizer(toggleCameraGestureRecognizer)
        // Zoom in : swipe screen from left to right
        let zoomInGestureRecognizer = UISwipeGestureRecognizer()
        zoomInGestureRecognizer.direction = .right
        zoomInGestureRecognizer.addTarget(self, action: #selector(zoomIn))
        view.addGestureRecognizer(zoomInGestureRecognizer)
        // Zoom in : swipe screen from right to left
        let zoomOutGestureRecognizer = UISwipeGestureRecognizer()
        zoomOutGestureRecognizer.direction = .left
        zoomOutGestureRecognizer.addTarget(self, action: #selector(zoomOut))
        view.addGestureRecognizer(zoomOutGestureRecognizer)
    }

    @objc func toggleCamera() {
        captureSession.beginConfiguration()

        // Change the device based on the current camera
        guard let newDevice = (currentDevice.position == AVCaptureDevice.Position.back ? frontFacingCamera : backingFacingCamera),
            // Change to the new input
            let cameraInput = try? AVCaptureDeviceInput(device: newDevice) else {
            return
        }

        // Remove all inputs from the session
        for input in captureSession.inputs where input .isKind(of: AVCaptureDeviceInput.self) {
            captureSession.removeInput(input)
        }

        if captureSession.canAddInput(cameraInput) {
            captureSession.addInput(cameraInput)
        }

        currentDevice = newDevice
        captureSession.commitConfiguration()
    }

    @objc func zoomIn() {
        let zoomFactor = currentDevice.videoZoomFactor
        if zoomFactor < 5.0 {
            let newZoomFactor = min(zoomFactor + 1.0, 5)
            do {
                try currentDevice.lockForConfiguration()
                currentDevice.ramp(toVideoZoomFactor: newZoomFactor, withRate: 1.0)
                currentDevice.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }
    @objc func zoomOut() {
        let zoomFactor = currentDevice.videoZoomFactor
        if zoomFactor > 1.0 {
            let newZoomFactor = max(zoomFactor - 1.0, 1.0)
            do {
                try currentDevice.lockForConfiguration()
                currentDevice.ramp(toVideoZoomFactor: newZoomFactor, withRate: 1.0)
                currentDevice.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }
    // MARK: - Action methods

    @IBAction func capture(sender: UIButton) {
        let settings = AVCapturePhotoSettings()
        settings.flashMode = .auto
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [
            kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
            kCVPixelBufferWidthKey as String: 160,
            kCVPixelBufferHeightKey as String: 160
        ]
        settings.previewPhotoFormat = previewFormat
        cameraOutput?.capturePhoto(with: settings, delegate: self)
    }

    // MARK: - Segues

    @IBAction func unwindToCameraView(segue: UIStoryboardSegue) {

    }

    // This method you can use somewhere you need to know camera permission   state
    func askPermission() {
        print("here")
        let cameraPermissionStatus =  AVCaptureDevice.authorizationStatus(for: AVMediaType.video)

        switch cameraPermissionStatus {
        case .authorized:
            print("Already Authorized")
        case .denied:
            print("denied")

            let alert = UIAlertController(title: "Sorry :(", message: "But  could you please grant permission for camera within device settings", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)

        case .restricted:
            print("restricted")
        default:
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { [weak self] (granted: Bool) -> Void in

                if granted == true {
                    // User granted
                    print("User granted")
                    DispatchQueue.main.async {
                        //Do smth that you need in main thread
                    }
                } else {
                    // User Rejected
                    print("User Rejected")

                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "WHY?", message: "Camera it is the main feature of our application", preferredStyle: .alert)
                        let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                        alert.addAction(action)
                        self?.present(alert, animated: true, completion: nil)
                    }
                }
            })
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPhoto",
            let photoViewController = segue.destination as? PhotoViewController {
            photoViewController.image = stillImage
        }
    }
}

extension SimpleCameraController: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let error = error {
            print("error occured : \(error.localizedDescription)")
        }
        if let dataImage = photo.fileDataRepresentation(),
            let dataProvider = CGDataProvider(data: dataImage as CFData) {
            let cgImageRef: CGImage! = CGImage(jpegDataProviderSource: dataProvider, decode: nil, shouldInterpolate: true, intent: .defaultIntent)
            let image = UIImage(cgImage: cgImageRef, scale: 1.0, orientation: .right)
            self.stillImage = image
            self.performSegue(withIdentifier: "showPhoto", sender: self)
        } else {
            print("some error")
        }
    }
}
